package com.example.jooqdemo.repository;

import com.example.jooqdemo.db.tables.records.StudentsRecord;
import com.example.jooqdemo.dto.StudentCreateRequestDto;
import com.example.jooqdemo.dto.StudentResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.jooq.InsertValuesStep6;
import org.jooq.JSON;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Result;
import org.jooq.SelectSelectStep;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

import static com.example.jooqdemo.db.Tables.CONTACTS;
import static com.example.jooqdemo.db.Tables.STUDENTS;
import static com.example.jooqdemo.db.Tables.STUDENTS_TAGS;
import static com.example.jooqdemo.db.Tables.TAGS;
import static org.jooq.impl.DSL.jsonArrayAgg;
import static org.jooq.impl.DSL.jsonEntry;
import static org.jooq.impl.DSL.jsonObject;

@Slf4j
@Repository
@RequiredArgsConstructor
public class StudentRepository {

    private final DSLContext dsl;

//    public Result<Record> getAll() {
//        Result<Record> fetch = dsl
//                .select(STUDENTS.fields())
//                .select(CONTACTS.fields())
//                .from(STUDENTS)
//                .leftJoin(CONTACTS).on(CONTACTS.ID.eq(STUDENTS.CONTACT_ID))
//                .orderBy(STUDENTS.ID.asc())
//                .fetch();
//        return fetch;
//    }

    public Result<Record6<Long, String, String, Long, String, String>> getStudentsInfo() {
        Result<Record6<Long, String, String, Long, String, String>> fetch = dsl.select(
                STUDENTS.ID,
                STUDENTS.NAME,
                STUDENTS.LAST_NAME,
                CONTACTS.ID,
                CONTACTS.BRANCH,
                CONTACTS.EMAIL
        )
                .from(STUDENTS)
                .leftJoin(CONTACTS).on(CONTACTS.ID.eq(STUDENTS.CONTACT_ID))
                .orderBy(STUDENTS.ID.asc())
                .fetch();
        return fetch;
    }

    public Result<StudentsRecord> getStudentsDetails() {
        return dsl.selectFrom(STUDENTS)
                .fetch();
    }

    public Long create(StudentCreateRequestDto requestDto) {
        Long contactId = Objects.requireNonNull(dsl.insertInto(
                CONTACTS,
                CONTACTS.BRANCH,
                CONTACTS.EMAIL,
                CONTACTS.FB,
                CONTACTS.INSTAGRAM,
                CONTACTS.LINKEDIN
        ).values(
                requestDto.getContact().getBranch(),
                requestDto.getContact().getEmail(),
                requestDto.getContact().getFb(),
                requestDto.getContact().getInstagram(),
                requestDto.getContact().getLinkedin()
        ).returningResult(CONTACTS.ID)
                .fetchOne()).value1();

        return Objects.requireNonNull(
                dsl.insertInto(
                        STUDENTS,
                        STUDENTS.NAME,
                        STUDENTS.ABOUT,
                        STUDENTS.DESCRIPTION,
                        STUDENTS.FATHER_NAME,
                        STUDENTS.LAST_NAME,
                        STUDENTS.SHORT_DESC,
                        STUDENTS.CONTACT_ID
                ).values(
                        requestDto.getName(),
                        requestDto.getAbout(),
                        requestDto.getDescription(),
                        requestDto.getFatherName(),
                        requestDto.getLastName(),
                        requestDto.getShortDesc(),
                        contactId
                ).returning(STUDENTS.ID).fetchOne()).value1();
    }

    public List<Long> batchInsert(List<StudentCreateRequestDto> requestDtoList) {
        InsertValuesStep6<StudentsRecord, String, String, String, String, String, String> query = dsl.insertInto(
                STUDENTS,
                STUDENTS.NAME,
                STUDENTS.ABOUT,
                STUDENTS.DESCRIPTION,
                STUDENTS.FATHER_NAME,
                STUDENTS.LAST_NAME,
                STUDENTS.SHORT_DESC
        );

        requestDtoList.forEach(requestDto -> query.values(
                requestDto.getName(),
                requestDto.getAbout(),
                requestDto.getDescription(),
                requestDto.getFatherName(),
                requestDto.getLastName(),
                requestDto.getShortDesc()
        ));

        return query.returning(STUDENTS.ID).fetch().getValues(STUDENTS.ID);
    }

    public Result<Record> getAll() {
        List<StudentResponseDto> studentResponseDtos = dsl
                .select(jsonObject(
                        jsonEntry("id", STUDENTS.ID),
                        jsonEntry("name", STUDENTS.NAME),
                        jsonEntry("lastName", STUDENTS.LAST_NAME),
                        jsonEntry("tags", DSL.field(
                                DSL.select(jsonArrayAgg(jsonObject(TAGS.ID, TAGS.NAME)))
                                        .from(TAGS)
                                        .leftJoin(STUDENTS_TAGS).on(STUDENTS.ID.eq(STUDENTS.ID))
                                        .leftJoin(TAGS).on(STUDENTS_TAGS.TAG_ID.eq(TAGS.ID))
                                        .where(STUDENTS_TAGS.STUDENT_ID.eq(STUDENTS.ID))
                        ))
                )).fetchInto(StudentResponseDto.class);
//                .select(CONTACTS.fields())
//                .select(TAGS.fields())
//                .from(STUDENTS)
//                .leftJoin(CONTACTS).on(CONTACTS.ID.eq(STUDENTS.CONTACT_ID))
//                .leftJoin(STUDENTS_TAGS).on(STUDENTS.ID.eq(STUDENTS.ID))
//                .leftJoin(TAGS).on(STUDENTS_TAGS.TAG_ID.eq(TAGS.ID))
//                .where(STUDENTS.ID.eq(1L))
//                .orderBy(STUDENTS.ID.asc())
//                .fetchGroups(
//                        record -> record.into(STUDENTS).into(StudentResponseDto.class)
//                );
        return null;
    }


//    @PostConstruct
//    public void getUser() {
//        Map<StudentResponseDto, List<TestDto>> studentResponseDtoListMap = dsl
//                .select(STUDENTS.fields())
//                .select(CONTACTS.fields())
//                .select(TESTS.fields())
//                .from(STUDENTS)
//                .join(CONTACTS).on(CONTACTS.ID.eq(STUDENTS.CONTACT_ID))
//                .leftJoin(TESTS).on(TESTS.STUDENT_ID.eq(STUDENTS.ID))
//                .where(STUDENTS.ID.eq(1L))
//                .fetchGroups(
//                        record -> record.into(STUDENTS).into(StudentResponseDto.class),
//                        record -> record.into(TESTS).into(TestDto.class)
//                );
//
//
//        System.out.println("a");
//
//    }
}
//    List<StudentResponseDto> studentResponseDtos = dsl
//            .select(jsonObject(
//                    jsonEntry("id", STUDENTS.ID),
//                    jsonEntry("name", STUDENTS.NAME),
//                    jsonEntry("lastName", STUDENTS.LAST_NAME),
//                    jsonEntry("tests", DSL.field(
//                            DSL.select(jsonArrayAgg(jsonObject(TESTS.ID, TESTS.NAME)))
//                                    .from(TESTS)
//                                    .where(TESTS.STUDENT_ID.eq(STUDENTS.ID))
//                    ))
//            ))
//            .from(STUDENTS)
//            .where(STUDENTS.ID.eq(1L)).fetchInto(StudentResponseDto.class);