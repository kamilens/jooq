package com.example.jooqdemo.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentCreateRequestDto {
    private String name;
    private String lastName;
    private String fatherName;
    private String about;
    private String description;
    private String shortDesc;
    private ContactCreateDto contact;
}
