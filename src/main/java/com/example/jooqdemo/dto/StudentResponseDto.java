package com.example.jooqdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentResponseDto {
    private Long id;
    private String name;
    private String lastName;
    private String fatherName;
    private String about;
    private String description;
    private String shortDesc;
    private ContactDto contact;
    private List<TagDto> tag;
}
