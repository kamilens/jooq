package com.example.jooqdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentInfoDto {
    private Long id;
    private String name;
    private String lastName;
    private Long contactId;
    private String contactBranch;
    private String contactEmail;
}
